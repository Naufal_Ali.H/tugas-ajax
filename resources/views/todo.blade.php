<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <div id="app">
    <h3>Todo List</h3>
    <input type="text">
    <ul>
        <li v-for="(index, todo) in todos">
            <span>@{{ todo,text }}</span>
            <button type="button">X</button>
            <button type="button">Done</button>
        </li>
    </ul>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
    <script>
        new Vue({
            el:"#app",
            data:{
                newTodo : "",
                todos : []
            }
        });
    </script>
    
</body>
</html>