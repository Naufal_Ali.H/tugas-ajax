<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ajax Request</title>
    <style>
        .changed{
            text-decoration: line-trough;
        }
    </style>
</head>
<body>
    
    <div id="app">
        <h3>User List</h3>
        <input type="text" v-model="newUser" @keyup.enter="addUser">
        <ul> 
            <li v-for="(user , index) in users" >
                <span v-bind:class="{'changed' : user.name}" >@{{ user.name }}</span>
                <button type="button" v-on:click="removeUser(index, name)">X</button
                >
                <button type="button" v-on:click="toggleChanged(user)">EDIT</button>
            </li>
        </ul>
        
    </div>

    <script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue-resource@1.5.1"></script>
    <script>
        new Vue({
            el: '#app',
            data: {
                newUser:'',
                users:[]
            },
            methods: {
                addUser: function () {
                    let nameInput = this.newUser.trim();
                    if(nameInput){

                        this.$http.post('api/user', {name: nameInput}).then(response => {
                            this.names.unshift(
                                { name : nameInput }
                            )
                            this.newUser = ''
                        });
                    }
                },
                removeUser: function (index, name) {
                    this.$http.post('api/user/delete/' + user.id).then(response => {
                        this.users.splice(index,1)    
                    });
                        
                },

                toggleChanged: function (user) {
                    this.$http.post('api/user/change-name/' + user.id).then(response => {
                        user.name = !user.name    
                    });
                
                }
            },
            mounted : function(){
                this.$http.get('api/user').then(response => {
                    let result = response.body.data;
                    this.users = result
                });
            }
        });
    </script>
    
</body>
</html>